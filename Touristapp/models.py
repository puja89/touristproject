from django.db import models

# Create your models here.


class Tourist(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    contact = models.CharField(max_length=50)
    email = models.EmailField()
    password = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class TourPackage(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='tours')
    location = models.CharField(max_length=200)
    amount = models.CharField(max_length=50)

    def __str__(self):
    	return self.title

