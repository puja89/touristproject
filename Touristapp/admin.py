from django.contrib import admin
from django.views.generic import *
from .models import *

# Register your models here.
admin.site.register(Tourist)
admin.site.register(TourPackage)
