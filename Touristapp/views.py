from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
import requests
# from django.contrib.messages.views import SuccessMessageMixin

# Create your views here.


class UserRequiredMixing(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.username == "deepak":
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class AboutView(UserRequiredMixing, TemplateView):
    template_name = 'about.html'


class TourPackageView(ListView):
    template_name = 'tourpackage.html'
    model = TourPackage
    context_object_name = 'allpackage'


class TourPackageDetailView(DetailView):
    template_name = 'tourpackage.html'
    model = TourPackage
    context_object_name = 'package'


class TourPackageCreateView(UserRequiredMixing, CreateView):
    template_name = 'tourpackagecreate.html'
    form_class = TourPackageForm
    success_url = "/"


class TourPackageDeleteView(UserRequiredMixing, DeleteView):
    template_name = "tourpackagedelete.html"
    model = TourPackage
    success_url = "/tourpackage/"


class TourPackageUpdateView(UserRequiredMixing, UpdateView):
    template_name = "studentcreate.html"
    form_class = TourPackageForm
    model = TourPackage
    success_url = "/tourpackage/"


class ContactUsView(TemplateView):
    template_name = 'contactus.html'


class SignUpView(TemplateView):
    template_name = 'signup.html'


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = "/"

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]
        user = authenticate(username=uname, password=pword)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, "login.html", {
                "form": form,
                "error": "Invalid username or password"
            })

        return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/login/")


class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegistrationForm
    success_url = '/login/'

    def form_valid(self, form):
        a = form.cleaned_data["username"]
        b = form.cleaned_data["email"]
        c = form.cleaned_data["password"]

        User.objects.create_user(a, b, c)

        return super().form_valid(form)


class BankApiView(TemplateView):
    template_name = 'bankapi.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url = "https://vast-shore-74260.herokuapp.com/banks?city=MUMBAI"
        resp = requests.get(url)
        data = resp.json()
        context["banks"] = data

        return context

# class ProtectedView(LoginRequiredMixin,TemplateView):
#     template_name='protectedpage.html'
#     login_url='/login/'


class ProtectedView(UserRequiredMixing, TemplateView):
    template_name = 'protectedpage.html'
