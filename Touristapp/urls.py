from django.urls import path
from .views import * 

app_name='Touristapp'
urlpatterns=[
  path('',HomeView.as_view(),name="home"),
  path('about/',AboutView.as_view(),name="about"),
  path('tour/',TourPackageView.as_view(),name="tourpackage"),
  path('tour/<int:pk>/detail/',TourPackageDetailView.as_view(),name="tourpackagedetail"),
  path('tourpackage/',TourPackageCreateView.as_view(),name="tourpackagecreate"),
  path('tourpackage/<int:pk>/edit/',TourPackageUpdateView.as_view(),name="tourpackageupdate"),
  path('tour/<int:pk>/delete/',TourPackageDeleteView.as_view(),name="tourpackagedelete"),
  path('contact/',ContactUsView.as_view(),name="contactus"),
  path('login/',LoginView.as_view(),name="login"),
  path('logout/',LogoutView.as_view(),name="logout"),
  path('register/',RegistrationView.as_view(),name="registration"),
  path('bankapi/',BankApiView.as_view(),name="bankapi"),
  path('protected-page/',ProtectedView.as_view(),name="protected"),

] 
