# Generated by Django 2.2.4 on 2019-09-17 09:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Touristapp', '0004_auto_20190911_1929'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tourpackage',
            name='image',
            field=models.ImageField(upload_to='tours'),
        ),
    ]
