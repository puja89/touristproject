from django import forms
from .models import *
from django.contrib.auth.models import User
from django.core import validators
import re

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }))


class TourPackageForm(forms.Form):
    class Meta:
        model = TourPackage
        fields = ['name', 'image', 'location', 'amount']


class RegistrationForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    def clean_username(self):
        uname = self.cleaned_data["username"]
        if User.objects.filter(username=uname).exists():
            raise forms.ValidationError(
                "user with this username already exists")

        return uname

    def clean_confirm_password(self):
        p1 = self.cleaned_data["password"]
        p2 = self.cleaned_data["confirm_password"]
        if not re.findall('\d',p1):
            raise forms.ValidationError("password must contain alphanumeric")
        if len(p1)<8 or len(p2)<8:
            raise forms.ValidationError("password is short")
        if p1 != p2:
            raise forms.ValidationError("password didnot match")

        return p2

  
    