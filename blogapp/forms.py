from django import forms
from .models import *
from django.contrib.auth.models import User

class BlogForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title','slug','image', 'content','views']