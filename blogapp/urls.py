from django.urls import path
from .views import * 


app_name='blogapp'
urlpatterns=[
 path('',BlogHomeView.as_view(),name="bloghome"),
 path('bloglist/',BlogListView.as_view(),name="bloglist"),
 path('blog/<int:pk>/detail/',BlogDetailView.as_view(),name="blogdetail"),
 path('blogcreate/',BlogCreateView.as_view(),name="blogcreate"),
 path('blog/<int:pk>/update/',BlogUpdateView.as_view(),name="blogupdate"),
 path('blog/<int:pk>/delete/',BlogDeleteView.as_view(),name="blogdelete"),

]