from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Article(models.Model):
	title=models.CharField(max_length=200)
	slug=models.SlugField(unique=True)
	image=models.ImageField(upload_to='blogs')
	content=models.TextField()
	date=models.DateTimeField(auto_now_add=True)
	author=models.ForeignKey(User,on_delete=models.CASCADE)
	views=models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.title
	"""docstring for """

class Comment(models.Model):
    article=models.ForeignKey(Article,on_delete=models.CASCADE)
    commenter=models.ForeignKey(User,on_delete=models.CASCADE)
    text=models.TextField()
    date=models.DateTimeField(auto_now_add=True)
    root=models.ForeignKey("self",on_delete=models.CASCADE,null=True,blank=True)
    #self le chai Comment lai call gareko ho 

    def __str__(self):
        return " comment for "+ self.article.title + " by " + self.commenter.username	