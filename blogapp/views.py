from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
# Create your views here.

class BlogHomeView(TemplateView):
	template_name="blogapp/bloghome.html"

class BlogListView(ListView):
    template_name="blogapp/bloglist.html"
    model=Article 
    context_object_name='allblog'

class BlogDetailView(DetailView):
    template_name = 'blogapp/blogdetail.html'
    model = Article
    context_object_name = 'blogde'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articlelist'] = Article.objects.all()

        article =self.object
        article.views += 1
        article.save()

        return context


class BlogCreateView(CreateView):
    template_name = 'blogapp/blogcreate.html'
    form_class = BlogForm
    success_url = "/"

    def dispatch(self,request,*args,**kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("Touristapp:login")
        return super().dispatch(request,*args,**kwargs)

    def form_valid(self,form):
        logged_in_user=self.request.user
        form.instance.author=logged_in_user

        return super().form_valid(form)                


class BlogDeleteView(DeleteView):
    template_name = "blogapp/blogdelete.html"
    model = Article
    success_url = "/bloglist/"


class BlogUpdateView(UpdateView):
    template_name = "blogapp/blogcreate.html"
    form_class = BlogForm
    model = Article
    success_url = "/bloglist/"
